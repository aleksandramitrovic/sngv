// Graph visualization diagrams
//import * as d3 from 'd3';

/*
data = {
    'nodes': [],
    'links': []
}

node = {
    'id': number,
    'avatar': url,
    'name': string,
    ... (other data that can be later shown in popup)
}
link = {
    source: ref to node,
    target: ref to node,
    strength: number
}

settings = {
    container: id where it should be added in html
    root: ref to central node
    width: number
    height: number
    radius: number // min(width, height) only for circular layout
    node: {
        size: number size for image
        maxdistance: number (for force directed layout)
    }
    link: {
        strokewidth: number
    }
    tooltip: {
        backgroundcolor: color
        border: border
        font: font
        imgsize: number
    }
    ...
}
*/

/**
 * Checks for required properties of data
 * @param {object} data 
 */
var checkData = function(data) {
    if (!data.hasOwnProperty('nodes')) {
        throw 'Nodes is an empty list!';
    } else if (data.nodes.length == 0) {
        throw 'Nodes is an empty list!';
    }
    if (!data.hasOwnProperty('links')) {
        data.links = [];
        console.warn('Links is an empty list!');
    } else if (data.links.length == 0) {
        console.warn('Links is an empty list!');
    }
}

/**
 * Checks for requered settings, and if something is missing, fills it width the default values.
 * @param {object} settings
 * @param {string} layout 
 */
var checkSettings = function(settings, layout) {
    if (!settings.hasOwnProperty('container')) {
        throw 'Container is required property for settings!'; // it must be provided
    }
    if (!settings.hasOwnProperty('width')) {
        settings.width = 640;
    }
    if (!settings.hasOwnProperty('height')) {
        settings.height = 480;
    }
    if (!settings.hasOwnProperty('node')) {
        settings.node = {size: 48};
    } else if (!settings.node.hasOwnProperty('size')) {
        settings.node.size = 48;
    }
    if (!settings.hasOwnProperty('link')) {
        settings.link = {strokewidth: 32};
    } else if (!settings.link.hasOwnProperty('strokewidth')) {
        settings.link.strokewidth = 32;
    }
    if (!settings.hasOwnProperty('tooltip')) {
        settings.tooltip = {
            backgroundcolor: 'snow',
            border: '1px solid gray',
            font: '12px Arial'
        }
    } else {
        if (!settings.tooltip.hasOwnProperty('backgroundcolor')) {
            settings.tooltip.backgroundcolor = 'snow';
        }
        if (!settings.tooltip.hasOwnProperty('border')) {
            settings.tooltip.border = '1px solid gray';
        }
        if (!settings.tooltip.hasOwnProperty('font')) {
            settings.tooltip.font = '12px Arial';
        }
    }
    // random layout specific
    if (layout === 'random') {

    }
    // matrix layout specific
    if (layout === 'matrix') {

    }
    // force directed layout specific
    if (layout === 'forceDirected') {
        if (!settings.node.hasOwnProperty('maxdistance')) {
            settings.node.maxdistance = 100;
        }
        if (!settings.node.hasOwnProperty('mindistance')) {
            settings.node.mindistance = 2*settings.node.size;
        }
    }
    // circular layout specific
    if (layout === 'circular') {
        if (!settings.hasOwnProperty('radius')) {
            settings.radius = Math.min(settings.width, settings.height);
        } else {
            settings.radius = Math.min(settings.width, settings.height, settings.radius);
        }
    }
}

var createNodeTooltip = function(settings) {
    var tooltipDiv = d3.select('body')
        .append('div')
        .attr('class', 'tooltip')
        .style('opacity', 0)
        .style('position', 'absolute')
        .style('padding', 2 + 'px')
        .style('background-color', settings.tooltip.backgroundcolor)
        .style('border', settings.tooltip.border)
        .style('border-radius', '5px')
        .style('font', settings.tooltip.font);
    var imageDiv = tooltipDiv.append('div')
        .attr('class', 'tooltipImage')
        .style('width', settings.tooltip.imgsize + 'px')
        .style('height', settings.tooltip.imgsize + 'px')
        .style('float', 'left')
    var image = imageDiv.append('img')
        .attr('src', '')
        .attr('width', settings.tooltip.imgsize)
        .attr('height', settings.tooltip.imgsize)
    var tooltipDataDiv = tooltipDiv.append('div')
        .attr('class', 'tooltipData')
        .style('float', 'left');
    var nameP = tooltipDataDiv.append('p')
        // top right bottom left
        .style('margin', '0px 5px 10px 5px');
    nameP.append('span').html('Name: ');
    var nameSpan = nameP.append('span');
    var usernameP = tooltipDataDiv.append('p')
        .style('margin', '0px 5px 10px 5px');
    usernameP.append('span').html('Username: ');
    var usernameSpan = usernameP.append('span');
    return {
        div: tooltipDiv,
        imageDiv: imageDiv,
        image: image,
        dataDiv: tooltipDataDiv,
        nameSpan: nameSpan,
        usernameSpan: usernameSpan
    };
};

var createLinkTooltip = function(settings) {
    var tooltipDiv = d3.select('body')
        .append('div')
        .attr('class', 'tooltip')
        .style('opacity', 0)
        .style('position', 'absolute')
        .style('padding', 2 + 'px')
        .style('background-color', settings.tooltip.backgroundcolor)
        .style('border', settings.tooltip.border)
        .style('border-radius', '5px')
        .style('font', settings.tooltip.font);
    // Source data
    var sdiv = tooltipDiv.append('div')
        .style('overflow', 'hidden');
    var simageDiv = sdiv.append('div')
        .attr('class', 'tooltipImage')
        .style('width', settings.tooltip.imgsize + 'px')
        .style('height', settings.tooltip.imgsize + 'px')
        .style('float', 'left')
    var simage = simageDiv.append('img')
        .attr('src', '')
        .attr('width', settings.tooltip.imgsize)
        .attr('height', settings.tooltip.imgsize)
    var stooltipDataDiv = sdiv.append('div')
        .attr('class', 'tooltipData')
        .style('float', 'left');
    var snameP = stooltipDataDiv.append('p')
        // top right bottom left
        .style('margin', '0px 5px 10px 5px');
    snameP.append('span').html('Name: ');
    var snameSpan = snameP.append('span');
    var susernameP = stooltipDataDiv.append('p')
        .style('margin', '0px 5px 10px 5px');
    susernameP.append('span').html('Username: ');
    var susernameSpan = susernameP.append('span');
    // Target data
    var tdiv = tooltipDiv.append('div')
        .style('overflow', 'hidden');
    var timageDiv = tdiv.append('div')
        .attr('class', 'tooltipImage')
        .style('width', settings.tooltip.imgsize + 'px')
        .style('height', settings.tooltip.imgsize + 'px')
        .style('float', 'left')
    var timage = timageDiv.append('img')
        .attr('src', '')
        .attr('width', settings.tooltip.imgsize)
        .attr('height', settings.tooltip.imgsize)
    var ttooltipDataDiv = tdiv.append('div')
        .attr('class', 'tooltipData')
        .style('float', 'left');
    var tnameP = ttooltipDataDiv.append('p')
        // top right bottom left
        .style('margin', '0px 5px 10px 5px');
    tnameP.append('span').html('Name: ');
    var tnameSpan = tnameP.append('span');
    var tusernameP = ttooltipDataDiv.append('p')
        .style('margin', '0px 5px 10px 5px');
    tusernameP.append('span').html('Username: ');
    var tusernameSpan = tusernameP.append('span');
    // Strength data
    var strengthP = tooltipDiv.append('p')
        .style('margin', '0px 5px 10px 5px');
    strengthP.append('span').html('Tie strength: ')
    var strengthSpan = strengthP.append('span');
    return {
        div: tooltipDiv,
        simage: simage,
        snameSpan: snameSpan,
        susernameSpan: susernameSpan,
        timage: timage,
        tnameSpan: tnameSpan,
        tusernameSpan: tusernameSpan,
        strengthSpan: strengthSpan
    };
};

var colorFromTieStrength = function (link) {
    var hue = 50;
    var lightness = 50;
    hue = 90;
    lightness = Math.round(90 - Math.abs(link.strength) * 40, 0);
    if (link.strength < 0) {
        hue = 0;
    }
    return "hsla(" + hue + ",100%," + lightness + "%,0.5)";
}

var randomLayout = function(data, settings) {
    // if settings is valid object continue
    try {
        checkSettings(settings, 'random');
    } catch (error) {
        console.error(error);
        return;
    }
    try {
        checkData(data);
    } catch (error) {
        console.error(error);
        return;
    }

    var layout = (function(data) {
        for(var i = 0; i < data.nodes.length; i++) {
            data.nodes[i].x = Math.random();
            data.nodes[i].y = Math.random();
        }
        for(i = 0; i < data.links.length; i++) {
            data.links[i].sxoffset = 0;
            data.links[i].txoffset = 0;
            data.links[i].syoffset = 0;
            data.links[i].tyoffset = 0;
        }
        for(i = 0; i < data.links.length-1; i++) {
            for(var j = i; j < data.links.length; j++) {
                if((data.links[i].source === data.links[j].target) && (data.links[i].target === data.links[j].source)) {
                    //1. for each link(line) find K
                    //1a. find the difference between points
                    var dx = data.links[i].target.x - data.links[i].source.x;
                    var dy = data.links[i].target.y - data.links[i].source.y;
                    var k = Math.atan2(dy, dx);
                    var y = Math.sin(k+Math.PI/2);
                    var x = Math.cos(k+Math.PI/2);

                    //stroke width / 2
                    var r = settings.link.strokewidth/2;

                    data.links[i].sxoffset = r*x;
                    data.links[i].syoffset = r*y;
                    data.links[i].txoffset = r*x;
                    data.links[i].tyoffset = r*y;
                    data.links[j].sxoffset = -r*x;
                    data.links[j].syoffset = -r*y;
                    data.links[j].txoffset = -r*x;
                    data.links[j].tyoffset = -r*y;
                }
            }
        }
        return data;
    })(data);

    var createLine = d3.line()
                        .x(function(node) {return node.x;})
                        .y(function(node) {return node.y;});

    // div for tooltip, initially is transparent
    var nodeTooltip = createNodeTooltip(settings);
    var linkTooltip = createLinkTooltip(settings);

    var svgElement = d3.select(settings.container)
        .append('svg')
        .attr('width', settings.width)
        .attr('height', settings.height);
    
    var defs = svgElement.append('defs');
    var pattern = defs.selectAll('pattern')
        .data(layout.nodes)
        .enter()
        .append('pattern')
        .attr('id', function(node) {return 'avatar_' + node.id;})
        .attr('width', '1')
        .attr('height', '1')
        .attr('patternUnits', 'objectBoundingBox');
    pattern.append('rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .style('fill', 'white')
        .style('stroke', 'none');
    pattern.append('svg:image')
        .attr('xlink:href', function(node) {return node.avatar;})
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .attr('x', 0)
        .attr('y', 0);

    var lineGroup = svgElement.append('g');
    var nodeGroup = svgElement.append('g');
    
    nodeGroup.selectAll('circle.node')
        .data(layout.nodes)
        .enter()
        .append('circle')
        .attr('cx', function(node) {return node.x * (settings.width-settings.node.size) + settings.node.size/2;})
        .attr('cy', function(node) {return node.y * (settings.height-settings.node.size) + settings.node.size/2;})
        .attr('r', settings.node.size/2)
        .attr('class', 'node')
        .style('fill', function(node) {return 'url(#avatar_' + node.id + ')';})
        .style('stroke', '#FF7F50')
        .on('mouseover', function(node) {
                nodeTooltip.image.attr('src', node.avatar);
                nodeTooltip.nameSpan.html(node.name);
                nodeTooltip.usernameSpan.html(node.username);
                nodeTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                nodeTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                nodeTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(node) {
                nodeTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );

    lineGroup.selectAll('path.link')
        .data(layout.links)
        .enter()
        .append('path')
        .attr('d', function(link) {return createLine([
            {
                'x': link.source.x*(settings.width-settings.node.size)+settings.node.size/2+link.sxoffset, 
                'y': link.source.y*(settings.height-settings.node.size)+settings.node.size/2+link.syoffset
            }, 
            {
                'x': link.target.x*(settings.width-settings.node.size)+settings.node.size/2+link.txoffset, 
                'y': link.target.y*(settings.height-settings.node.size)+settings.node.size/2+link.tyoffset
            }
        ]);})
        .attr('class', 'link')
        .attr('stroke-width', settings.link.strokewidth)
        .attr('stroke', function(link) {return colorFromTieStrength(link)})
        .on('mouseover', function(link) {
                linkTooltip.simage.attr('src', link.source.avatar);
                linkTooltip.snameSpan.html(link.source.name);
                linkTooltip.susernameSpan.html(link.source.username);
                linkTooltip.timage.attr('src', link.target.avatar);
                linkTooltip.tnameSpan.html(link.target.name);
                linkTooltip.tusernameSpan.html(link.target.username);
                linkTooltip.strengthSpan.html(link.strength);
                linkTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                linkTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                linkTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(link) {
                linkTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );
};

var matrixLayout = function(data, settings) {
    try {
        checkSettings(settings, 'matrix');
    } catch (error) {
        console.error(error);
        return;
    }
    try {
        checkData(data);
    } catch (error) {
        console.error(error);
        return;
    }

    var layout = (function (data) {
        var matrix = [];
        for(var r = 0; r < data.nodes.length; r++) {
            for(var c = 0; c < data.nodes.length; c++) {
                var connetion = {
                    source: data.nodes[r],
                    target: data.nodes[c],
                    strength: NaN,
                    row: r,
                    column: c
                };
                for(var i = 0; i < data.links.length; i++) {
                    if ((data.links[i].source == data.nodes[r]) && (data.links[i].target == data.nodes[c])) {
                       connetion.strength = data.links[i].strength;
                    }
                }
                matrix.push(connetion);
            }
            data.nodes[r].x = r;
        }
        return matrix;
    })(data);

    var calculateTextPosition = function(node) {
        return {
            x: data.nodes.length*settings.node.size + data.nodes.length*1,
            y: node.x*settings.node.size+settings.node.size/2 + node.x*1
        }
    }

    var calculatePosition = function(element) {
        return {
            y: element.row*settings.node.size + element.row*1,
            x: element.column*settings.node.size + element.column*1,
        }
    }

    var calculateColor = function(element) {
        if (isNaN(element.strength)) {
            return 'whitesmoke';
        } else {
            return colorFromTieStrength(element);
        }
    }

    var linkTooltip = createLinkTooltip(settings);

    var svgElement = d3.select(settings.container)
        .append('svg')
        .attr('width', settings.width)
        .attr('height', settings.height);
    
    var nodeGroup = svgElement.append('g');

    nodeGroup.selectAll('rect.cell')
        .data(layout)
        .enter()
        .append('rect')
        .attr('class', 'cell')
        .attr('x', function(element) {return calculatePosition(element).x;})
        .attr('y', function(element) {return calculatePosition(element).y;})
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .style('fill', function(element) {return calculateColor(element);})
        .on('mouseover', function(element) {
            linkTooltip.simage.attr('src', element.source.avatar);
            linkTooltip.snameSpan.html(element.source.name);
            linkTooltip.susernameSpan.html(element.source.username);
            linkTooltip.timage.attr('src', element.target.avatar);
            linkTooltip.tnameSpan.html(element.target.name);
            linkTooltip.tusernameSpan.html(element.target.username);
            linkTooltip.strengthSpan.html(element.strength);
            linkTooltip.div.style('left', 10 + d3.event.pageX + 'px')
            linkTooltip.div.style('top', 10 + d3.event.pageY + 'px')
            linkTooltip.div.transition()
                .duration(200)
                .style('opacity', 0.85)
                .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(node) {
                linkTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );
    /*
    nodeGroup.selectAll('text')
        .data(layout)
        .enter()
        .append('text')
        .attr('x', function(element) {return calculatePosition(element).x;})
        .attr('y', function(element) {return calculatePosition(element).y;})
        .text(function(element) {return element.source.id +" "+ element.target.id})
        .attr('alignment-baseline', 'hanging')
    */
    var rowNames = svgElement.append('g');

    rowNames.selectAll('text.row')
        .data(data.nodes)
        .enter()
        .append('text')
        .attr('class', 'row')
        .attr('x', function(node) {return calculateTextPosition(node).x;})
        .attr('y', function(node) {return calculateTextPosition(node).y;})
        .attr('alignment-baseline', 'middle')
        .text(function(node) {return node.name});

    var columnNames = svgElement.append('g');
    columnNames.selectAll('text.column')
        .data(data.nodes)
        .enter()
        .append('text')
        .attr('class', 'column')
        .attr('x', function(node) {return calculateTextPosition(node).y;})
        .attr('y', function(node) {return calculateTextPosition(node).x;})
        .attr('alignment-baseline', 'middle')
        .attr('transform', function(node) {return 'rotate(90,'+calculateTextPosition(node).y+','+calculateTextPosition(node).x+')';})
        .text(function(node) {return node.name});
};

var forceDirectedLayout = function(data, settings) {
    try {
        checkSettings(settings, 'forceDirected');
    } catch (error) {
        console.error(error);
        return;
    }
    try {
        checkData(data);
    } catch (error) {
        console.error(error);
        return;
    }

    var layout = (function(data) {
        for(var i = 0; i < data.nodes.length; i++) {
            data.nodes[i].x = Math.random();
            data.nodes[i].y = Math.random();
        }
        for(i = 0; i < data.links.length; i++) {
            data.links[i].sxoffset = 0;
            data.links[i].txoffset = 0;
            data.links[i].syoffset = 0;
            data.links[i].tyoffset = 0;
        }
        for(i = 0; i < data.links.length-1; i++) {
            for(var j = i; j < data.links.length; j++) {
                if((data.links[i].source === data.links[j].target) && (data.links[i].target === data.links[j].source)) {
                    //1. for each link(line) find K
                    //1a. find the difference between points
                    var dx = data.links[i].target.x - data.links[i].source.x;
                    var dy = data.links[i].target.y - data.links[i].source.y;
                    var k = Math.atan2(dy, dx);
                    var y = Math.sin(k+Math.PI/2);
                    var x = Math.cos(k+Math.PI/2);

                    //stroke width / 2
                    var r = settings.link.strokewidth/2;

                    data.links[i].sxoffset = r*x;
                    data.links[i].syoffset = r*y;
                    data.links[i].txoffset = r*x;
                    data.links[i].tyoffset = r*y;
                    data.links[j].sxoffset = -r*x;
                    data.links[j].syoffset = -r*y;
                    data.links[j].txoffset = -r*x;
                    data.links[j].tyoffset = -r*y;
                }
            }
        }
        return data;
    })(data);

    var createLine = d3.line()
        .x(function(node) {return node.x;})
        .y(function(node) {return node.y;});

    // div for tooltip, initially is transparent
    var nodeTooltip = createNodeTooltip(settings);
    var linkTooltip = createLinkTooltip(settings);

    var svgElement = d3.select(settings.container)
        .append('svg')
        .attr('width', settings.width)
        .attr('height', settings.height);
    
    var defs = svgElement.append('defs');
    var pattern = defs.selectAll('pattern')
        .data(layout.nodes)
        .enter()
        .append('pattern')
        .attr('id', function(node) {return 'avatar_' + node.id;})
        .attr('width', '1')
        .attr('height', '1')
        .attr('patternUnits', 'objectBoundingBox');
    pattern.append('rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .style('fill', 'white')
        .style('stroke', 'none');
    pattern.append('svg:image')
        .attr('xlink:href', function(node) {return node.avatar;})
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .attr('x', 0)
        .attr('y', 0);

    var lineGroup = svgElement.append('g');
    var nodeGroup = svgElement.append('g');
    
    var nodes = nodeGroup.selectAll('circle.node')
        .data(layout.nodes)
        .enter()
        .append('circle')
        .attr('cx', function(node) {return node.x * (settings.width-settings.node.size) + settings.node.size/2;})
        .attr('cy', function(node) {return node.y * (settings.height-settings.node.size) + settings.node.size/2;})
        .attr('r', settings.node.size/2)
        .attr('class', 'node')
        .style('fill', function(node) {return 'url(#avatar_' + node.id + ')';})
        .style('stroke', '#FF7F50')
        .on('mouseover', function(node) {
                nodeTooltip.image.attr('src', node.avatar);
                nodeTooltip.nameSpan.html(node.name);
                nodeTooltip.usernameSpan.html(node.username);
                nodeTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                nodeTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                nodeTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(node) {
                nodeTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        )
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

        var links = lineGroup.selectAll('path.link')
        .data(layout.links)
        .enter()
        .append('path')
        .attr('d', function(link) {return createLine([
            {
                'x': link.source.x*(settings.width-settings.node.size)+settings.node.size/2+link.sxoffset, 
                'y': link.source.y*(settings.height-settings.node.size)+settings.node.size/2+link.syoffset
            }, 
            {
                'x': link.target.x*(settings.width-settings.node.size)+settings.node.size/2+link.txoffset, 
                'y': link.target.y*(settings.height-settings.node.size)+settings.node.size/2+link.tyoffset
            }
        ]);})
        .attr('class', 'link')
        .attr('stroke-width', settings.link.strokewidth)
        .attr('stroke', function(link) {return colorFromTieStrength(link)})
        .on('mouseover', function(link) {
                linkTooltip.simage.attr('src', link.source.avatar);
                linkTooltip.snameSpan.html(link.source.name);
                linkTooltip.susernameSpan.html(link.source.username);
                linkTooltip.timage.attr('src', link.target.avatar);
                linkTooltip.tnameSpan.html(link.target.name);
                linkTooltip.tusernameSpan.html(link.target.username);
                linkTooltip.strengthSpan.html(link.strength);
                linkTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                linkTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                linkTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(link) {
                linkTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );

    // Force simulation
    var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().strength(0.1).distance(function(link) {
        return ((1-link.strength)/2) * settings.node.maxdistance + settings.node.mindistance}))
    .force("charge", d3.forceManyBody().strength(-30))
    .force("center", d3.forceCenter(settings.width / 2, settings.height / 2));

    simulation
        .nodes(layout.nodes)
        .on("tick", ticked);

    simulation.force("link")
        .links(layout.links);

    function ticked() {
        links
            .attr('d', function(link) {return createLine([
                {
                    'x': link.source.x+link.sxoffset, 
                    'y': link.source.y+link.syoffset
                },
                {
                    'x': link.target.x+link.txoffset, 
                    'y': link.target.y+link.tyoffset
                }
            ]);})
        
        nodes
            .attr("cx", function(node) {return node.x;})
            .attr("cy", function(node) {return node.y;});
    }

    function dragstarted(node) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        node.fx = node.x;
        node.fy = node.y;
      }
      
      function dragged(node) {
        node.fx = d3.event.x;
        node.fy = d3.event.y;
      }
      
      function dragended(node) {
        if (!d3.event.active) simulation.alphaTarget(0);
        node.fx = null;
        node.fy = null;
      }
};

var circularLayout = function(data, settings) {
    try {
        checkSettings(settings, 'circular');
    } catch (error) {
        console.error(error);
        return;
    }
    try {
        checkData(data);
    } catch (error) {
        console.error(error);
        return;
    }

    var layout = (function (data) {
        for (var i = 0; i < data.nodes.length; i++) {
            data.nodes[i].x = Math.cos(2*Math.PI - (2*Math.PI)/data.nodes.length*i);
            data.nodes[i].y = Math.sin(2*Math.PI - (2*Math.PI)/data.nodes.length*i);
        }
        for(i = 0; i < data.links.length; i++) {
            data.links[i].sxoffset = 0;
            data.links[i].txoffset = 0;
            data.links[i].syoffset = 0;
            data.links[i].tyoffset = 0;
        }
        for(i = 0; i < data.links.length-1; i++) {
            for(var j = i; j < data.links.length; j++) {
                if((data.links[i].source === data.links[j].target) && (data.links[i].target === data.links[j].source)) {
                    //1. for each link(line) find K
                    //1a. find the difference between points
                    var dx = data.links[i].target.x - data.links[i].source.x;
                    var dy = data.links[i].target.y - data.links[i].source.y;
                    var k = Math.atan2(dy, dx);
                    var y = Math.sin(k+Math.PI/2);
                    var x = Math.cos(k+Math.PI/2);

                    //stroke width / 2
                    var r = settings.link.strokewidth/2;

                    data.links[i].sxoffset = r*x;
                    data.links[i].syoffset = r*y;
                    data.links[i].txoffset = r*x;
                    data.links[i].tyoffset = r*y;
                    data.links[j].sxoffset = -r*x;
                    data.links[j].syoffset = -r*y;
                    data.links[j].txoffset = -r*x;
                    data.links[j].tyoffset = -r*y;
                }
            }
        }
        return data;
    })(data);

    var createLine = d3.line()
                        .x(function(node) {return node.x;})
                        .y(function(node) {return node.y;})
                        //.curve(d3.curveBasis);
                        // FIXME: delete curve to get straight line


    var nodeTooltip = createNodeTooltip(settings);
    var linkTooltip = createLinkTooltip(settings);

    var svgElement = d3.select(settings.container)
        .append('svg')
        .attr('width', settings.width)
        .attr('height', settings.height);
    
    var defs = svgElement.append('defs');
    var pattern = defs.selectAll('pattern')
        .data(layout.nodes)
        .enter()
        .append('pattern')
        .attr('id', function(node) {return 'avatar_' + node.id;})
        .attr('width', '1')
        .attr('height', '1')
        .attr('patternUnits', 'objectBoundingBox');
    pattern.append('rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .style('fill', 'white')
        .style('stroke', 'none');
    pattern.append('svg:image')
        .attr('xlink:href', function(node) {return node.avatar;})
        .attr('width', settings.node.size)
        .attr('height', settings.node.size)
        .attr('x', 0)
        .attr('y', 0);

    var lineGroup = svgElement.append('g');
    var nodeGroup = svgElement.append('g');
    
    nodeGroup.selectAll('circle.node')
        .data(layout.nodes)
        .enter()
        .append('circle')
        .attr('cx', function(node) {return node.x * (settings.radius/2 - settings.node.size) + (settings.width/2);})
        .attr('cy', function(node) {return node.y * (settings.radius/2 - settings.node.size) + (settings.height/2);})
        .attr('r', settings.node.size/2)
        .attr('class', 'node')
        .style('fill', function(node) {return 'url(#avatar_' + node.id + ')';})
        .style('stroke', '#FF7F50')
        .on('mouseover', function(node) {
                nodeTooltip.image.attr('src', node.avatar);
                nodeTooltip.nameSpan.html(node.name);
                nodeTooltip.usernameSpan.html(node.username);
                nodeTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                nodeTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                nodeTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(node) {
                nodeTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );

    lineGroup.selectAll('path.link')
        .data(layout.links)
        .enter()
        .append('path')
        .attr('d', function(link) {return createLine([
            {
                'x': link.source.x*(settings.radius/2 - settings.node.size) + (settings.width/2)+link.sxoffset, 
                'y': link.source.y*(settings.radius/2 - settings.node.size) + (settings.height/2)+link.syoffset
            },/* // this is for curved lines
            {
                'x': settings.width/2,
                'y': settings.height/2
            },*/ 
            {
                'x': link.target.x*(settings.radius/2 - settings.node.size) + (settings.width/2)+link.txoffset,
                'y': link.target.y*(settings.radius/2 - settings.node.size) + (settings.height/2)+link.tyoffset
            }
        ])})
        .attr('class', 'link')
        .attr('stroke-width', settings.link.strokewidth)
        .attr('stroke', function(link) {return colorFromTieStrength(link)})
        .style('fill', 'none')
        .on('mouseover', function(link) {
                linkTooltip.simage.attr('src', link.source.avatar);
                linkTooltip.snameSpan.html(link.source.name);
                linkTooltip.susernameSpan.html(link.source.username);
                linkTooltip.timage.attr('src', link.target.avatar);
                linkTooltip.tnameSpan.html(link.target.name);
                linkTooltip.tusernameSpan.html(link.target.username);
                linkTooltip.strengthSpan.html(link.strength);
                linkTooltip.div.style('left', 10 + d3.event.pageX + 'px')
                linkTooltip.div.style('top', 10 + d3.event.pageY + 'px')
                linkTooltip.div.transition()
                    .duration(200)
                    .style('opacity', 0.85)
                    .style('visibility', 'visible');
            }
        )
        .on('mouseout', function(link) {
                linkTooltip.div.transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('visibility', 'hidden');
            }
        );
};
